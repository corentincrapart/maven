# TP Maven et Intégration continue

## Projet Maven
Le projet maven consiste à créer une application de type SpringBoot qui permet de calculer le nombre de Fibonacci d'un entier quelconque.


Une application Spring est centrée sur une classe contenant le main exécuté lors de la mise en fonction de l'application. Cette classe est reliée à un une classe de contrôle qui fait le lien entre une page web et classe java par le biais de code javascript. Et c'est la classe java qui fait alors le calcul du nombre de Fibonacci.  


Pour exécuter l'application on peut passer par un IDE et la démarrer en tant que Java Application ; passer par un terminal et exécuter le .jar correspondant ou lancer la commande curl.


## CI/CD


Pour mettre en place l'intégration continue sur le projet je suis passé par GitLab CI qui permet de centraliser tous les éléments du projet. En effet, GitLab fait office de dépôt et d'outil d'intégration continue.  


Cependant, un Inconvénient majeur de l'outil GitLab CI est son côté non user-friendly, c'est à dire que GitLab à le problème de n'être pas du tout intuitif contrairement à Jenkins qui l'est beaucoup plus. Effectivement, le paramétrage de cet outil est beaucoup moins clair qu'il ne pourrait l'être avec l'utilisation d'une interface graphique telle que celle de Jenkins. J'ai également essayé TeamCity mais ce dernier ne donnant pas de résultat convaincant je suis resté à l'utilisation de GitLab CI.
